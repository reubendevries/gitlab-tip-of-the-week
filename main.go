package main

import (
	"log"
	"net"
)

func main() {
	listen, err := net.Listen("tcp", ":8000")
	if err != nil {
		log.Fatalf("error:%v", err)
	}
}
